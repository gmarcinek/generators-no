let generators = require('yeoman-generator');
let _ = require('lodash');
let memFs = require('mem-fs');
let editor = require('mem-fs-editor');
let store = memFs.create();
let fs = editor.create(store);
let options = {};

module.exports = generators.Base.extend({ 
  initializing,
  writing,
  end: () => console.log('DONE')
});

function initializing () {
  this.argument('name', { type: String, required: true });
  this.name = _.camelCase(this.name);
  this.copyFile = (src, dest, opt=options) => this.fs.copyTpl(this.templatePath(src), this.destinationPath(dest), opt);

  options = {
    name: this.name,
    nameKebab: _.kebabCase(this.name),
    nameCapitalized: this.name.substr(0, 1).toUpperCase() + this.name.substr(1)
  }
}

function writing() {
  this.copyFile('component/module.js', `./src/components/${options.nameKebab}/${options.nameKebab}-module.js`);
  this.copyFile('component/controller.js', `src/components/${options.nameKebab}/${options.nameKebab}-controller.js`);
  this.copyFile('component/directive.js', `src/components/${options.nameKebab}/${options.nameKebab}-directive.js`);
  this.copyFile('component/service.js', `src/components/${options.nameKebab}/${options.nameKebab}-service.js`);
  this.copyFile('component/view.html', `src/components/${options.nameKebab}/${options.nameKebab}.html`);
}
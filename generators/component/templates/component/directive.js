'use strict';

import angular from 'angular';

export default function <%= name %>Directive() {
  return {
    restrict: 'E',
    controller: '<%= nameCapitalized %>Controller as <%= name %>Ctrl',
    templateUrl: 'components/<%= nameKebab %>/<%= nameKebab %>.html',
    scope: true,
    bindToController: {},
    link: postLink
  }

  function postLink(scope, attrs, element, ctrl) {
  	// body
	}
}; 
<%= name %>Directive.$inject = [];
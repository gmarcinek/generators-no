'use strict';

import angular from 'angular';
import <%= nameCapitalized %>Controller from './<%= nameKebab %>-controller.js';
import <%= nameCapitalized %>Service from './<%= nameKebab %>-service.js';
import <%= name %>Directive from './<%= nameKebab %>-directive.js';

export default angular.module('<%= name %>', [
	/* dependencies */
])
.controller('<%= nameCapitalized %>Controller', <%= nameCapitalized %>Controller)
.service('<%= name %>Service', <%= nameCapitalized %>Service)
.directive('<%= name %>', <%= name %>Directive);
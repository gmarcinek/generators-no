'use strict';

import angular from 'angular';

export default class <%= nameCapitalized %>Controller {
	constructor() {}
}
<%= nameCapitalized %>Controller.$inject = [];